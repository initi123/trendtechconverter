import {Converter} from "../Converter.js";

describe('Массив в строку', function () {
    describe('Успешные тесты', function () {
        it("1-8", async function () {
            let expectedValue = '1-8';
            let arrayForCheck = [1, 2, 3, 4, 5, 6, 7, 8];
            let promise;

            try {
                promise = new Converter().arrayToString(arrayForCheck);
            } catch (e) {
                //
            }

            await promise.then(
                resolve => {
                    if (expectedValue !== resolve) {
                        throw new Error(`Ожидалось ${expectedValue}, получили ${resolve}`);
                    }
                }
            );
        });

        it("1-6,100,1091,1999-2002", async function () {
            let expectedValue = '1-6,100,1091,1999-2002';
            let arrayForCheck = [1,2,3,4,5,6,100,1091,1999,2000,2001,2002];
            let promise;

            try {
                promise = new Converter().arrayToString(arrayForCheck);
            } catch (e) {
                //
            }

            await promise.then(
                resolve => {
                    if (expectedValue !== resolve) {
                        throw new Error(`Ожидалось ${expectedValue}, получили ${resolve}`);
                    }
                }
            );
        });

        it("1-3,7-9,15,17,19-21", async function () {
            let expectedValue = '1-3,7-9,15,17,19-21';
            let arrayForCheck = [1,2,3,7,8,9,15,17,19,20,21];
            let promise;

            try {
                promise = new Converter().arrayToString(arrayForCheck);
            } catch (e) {
                //
            }

            await promise.then(
                resolve => {
                    if (expectedValue !== resolve) {
                        throw new Error(`Ожидалось ${expectedValue}, получили ${resolve}`);
                    }
                }
            );
        });
    });

    describe('Обработанные тесты с ошибками', function () {
        it("Ошибка целостности массива: Массив должен быть отсортированным по возрастанию", async function () {
            let expectedValue = 'Ошибка целостности массива: Массив должен быть отсортированным по возрастанию';
            let arrayForCheck = [1, 2, 1, 4, 5, 6, 7, 8];

            try {
                await new Converter().arrayToString(arrayForCheck);
            } catch (e) {
                if (e.message !== expectedValue) {
                    throw new Error(`Ожидалось: "${expectedValue}", получили: "${e.message}"`)
                }
            }
        });

        it("Ошибка целостности массива: Все элементы массива должны быть целыми числами", async function () {
            let expectedValue = 'Ошибка целостности массива: Все элементы массива должны быть целыми числами';
            let arrayForCheck = [1, 2, 3.12, 4, 5, 6, 7, 8];

            try {
                await new Converter().arrayToString(arrayForCheck);
            } catch (e) {
                if (e.message !== expectedValue) {
                    throw new Error(`Ожидалось: "${expectedValue}", получили: "${e.message}"`)
                }
            }
        });

        it("Ошибка целостности массива: Все элементы массива должны быть больше нуля", async function () {
            let expectedValue = 'Ошибка целостности массива: Все элементы массива должны быть больше нуля';
            let arrayForCheck = [-1, 1, 2, 3, 4, 5, 6, 7, 8];

            try {
                await new Converter().arrayToString(arrayForCheck);
            } catch (e) {
                if (e.message !== expectedValue) {
                    throw new Error(`Ожидалось: "${expectedValue}", получили: "${e.message}"`)
                }
            }
        });
    });
});
