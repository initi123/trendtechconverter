export class Converter {
    /**
     * @example [1, 2, 3, 4, 5, 6, 7, 8] => "1-8"
     *
     * @returns {Promise<string>}
     */
    async arrayToString(array) {
        const arrayLength = array.length;

        let startIndex = 0;
        let formattedString = '';

        for (let i = 0; i < arrayLength; i++) {
            if (!Number.isInteger(array[i])) {
                throw new Error(this.integrityErrorMessage('Все элементы массива должны быть целыми числами'));
            } else if (0 > array[i]) {
                throw new Error(this.integrityErrorMessage('Все элементы массива должны быть больше нуля'));
            } else if (array[i + 1] <= array[i]) {
                throw new Error(this.integrityErrorMessage('Массив должен быть отсортированным по возрастанию'));
            }

            if (1 === array[i + 1] - array[i]) {
                continue;
            }

            formattedString = startIndex === i
                ? formattedString += array[i]
                : formattedString += array[startIndex] + '-' + array[i];

            if (i + 1 >= arrayLength) {
                break;
            }

            formattedString += ',';
            startIndex = i + 1;
        }

        return formattedString;
    }

    /**
     * @param message
     * @returns {string}
     */
    integrityErrorMessage(message)
    {
        return 'Ошибка целостности массива: ' + message;
    }
}